
# 实现方式
使用MyBatis提供的标签来实现`<if>`、`<where>`、`<foreach>`等

# 标签
## `<if>`
用法：
&emsp;&emsp;`<if test="Java对象属性的判断">`  
比如接口方法传入的是一个Student对象，它有String name属性  
那么判断规则可以写成**"name != null and name != ''"**用来确定该属性有值且不是空字符串  

## `<where>`
\<where\>可以用来包含多个\<if\>，当有为真的if时，\<where\>标签可以给语句自动加上一个where关键字，并去掉\<if\>标签中不需要的AND/OR等连接符  
使用它可以避免单纯用\<if\>标签时可能出错的情况  

## `<foreach>`
用来循环Java的集合类对象（*数组*、*List*）  
主要用来处理SQL语句中使用了`in`关键字的情况  
```
<foreach collection="" item="" open="" close="" separator="">
    <!--
        collection: 接口中方法的参数类型，数组使用array，List使用list
        item: 自定义，用来代指foreach操作的数组/集合中的元素
        open: 循环开始的字符
        close: 循环结束的字符
        separator: 数组/集合中成员之间的分隔符
    -->
</foreach>
```

## 代码片段
使用`<sql>`标签来创建可以重复利用的SQL语句片段  
```
<sql id="THE_FRAGMENT_ID">
    select ...
</sql>

<include refid="THE_FRAGMENT_ID_YOU_DEFINED" />
<where>
    ...
</where>
```

