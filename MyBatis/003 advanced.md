
# Java代码和xml文件之间的传参
在xml标签中使用`parameterType`来指定传入参数的类型
```
<select id="selectSomeData" parameterType="Java.lang.Integer" resultType="org.javalearn.domain.Student">
    select id, name, email, age from student where id = #{id}
</select>
```
*parameterType*的值，可以使用Java类的全限定名称，可以使用MyBatis内置的别名  
它也不是强制的，不设定时，MyBatis通过反射机制来进行发现  

## 传入<u>一个</u> <u>简单</u>参数
*简单参数*理解为 Java基本数据类型 或者 String数据类型  
传入一个简单参数使用`#{PARAM_NAME}`的方式，*PARAM_NAME*为占位符，名称随意，符合命名规范即可  
`#{}`使用`PreparedStatement`对象来进行SQL语句处理  

## 传入多个参数
### Java中使用@Param("参数名")方式
Java
```
public List<Student> selectMultiStudents(@Param("myName") String name, @Param("myAge") int age) {}
```
mapper 文件
```
<select>
    select * from student where name=#{myName} or age = #{myAge}
</select>
```

### 使用对象传参
Java代码中方法会传入一个对象  
mapper文件则使用`#{对象属性名, javaType=该属性在Java中的数据类型, jdbcType=MyBatis支持的JDBC数据类型}`的语法格式  
上文是完整语法形式，简化直接使用`#{对象属性名}`即可，后面两项由MyBatis通过反射自行获取  

### 位置传参（不推荐）
使用参数位置来传递参数  
MyBatis3.3及之前版本使用`#{0}`、`#{1}`这种格式  
3.4开始使用`#{arg0}`格式  
<sup>不推荐理由：</sup>0、1这种数字方式可读性差（没法让人知道各参数代表的啥）；与方法的形参顺序关联过于密切，一旦修改Java代码，mapper文件可能需要随之变动  

### 使用Map（不推荐）
接口方法接收Map对象  
mapper文件中使用`#{KEY_NAME}`进行调用  
<sup>不推荐理由：</sup>Map的key命名没有约束；从形参Map看不出需要哪些参数，反射也无法获取具体参数  

#### 什么时候会用Map？
做多表联查时，<sup>这里是我自己的理解：</sup>如果不想为查询多创建一个类，可以使用Map
```
select * from tbl_student s left join tbl_class c on s.classId=c.id where s.name = #{SNAME} and c.name=#{CNAME}
```

## 占位符\#和$的区别
如上文述，`#{}`使用的是`PreparedStatement`  
`${}`则是直接使用字符替代所在位置，使用的是`Statement`，多用在替换**表名**、**列名**、**列排序**等场景  

# 返回结果
## resultType
mapper文件中的`resultType`可以使用Java全限定名称，可以使用MyBatis预设的Java类别名（详见MyBatis文档）  
也可以进行用户自定义  
在MyBatis主配置文件中
```
<typeAliases>
    <!-- 针对具体类进行别名定义 -->
    <typeAlias type="JAVA_CLASS_FULL_NAME" alias="THE_ALIAS_YOU_WANT" />

    <!-- 针对包进行导入，包中类的名字就是它的别名 -->
    <package name="THE_PACKAGE_YOU_WANT_TO_IMPORT" />
</typeAliases>
```
resultType可以是`简单数据类型`、`引用数据类型`、`Map`<sup>个人补充：Map其实也是引用数据类型，这里写出应该是它在resultType中的常用性</sup>

## resultMap
用来做`数据库表列名`和`Java代码中属性名`的映射关系  
在mapper文件中
```
<resultMap id="A_CUSTOMIZED_RESULT_MAP" type="A_JAVA_CLASS_FULL_NAME">
    <!-- 主键列使用 id -->
    <id column="DATABASE_COLUMN_NAME" property="JAVA_FIELD_NAME" />
    <!-- 非主键列使用 result -->
    <result column="DATABASE_COLUMN_NAME" property="JAVA_FIELD_NAME" />
</resultMap>

<select id="SELECT_ID" resultMap="RESULTMAP_ID">

</select>
```
除了使用resultMap来处理Java代码中的属性和数据表中的列名不一致的问题外，还可以在mapper文件中的`<select>`标签内使用`as`关键字定义查询语句列的别名，从而实现两者的对应关系  

# 模糊查询
## Java代码中指定'like'的内容
*DAO interface*
```
Student selectStudentByLike(String name) {}
```
*mapper*
```
<select>
    select ... where name like #{key}
</select>
```
*使用*
```
public static void main(String[] args) {
    String key = "THE_QUERY_KEY";
    DAO_INTERFACE_NAME mapper = sqlSession.getMapper(DAO_CLASS_NAME.class);

    mapper.selectStudentByLike(key);
}
```

## 在mapper文件中拼接'like'的内容
```
<select>
    selet *** where name like "%" #{key} "%"
</select>
```
<sup>⚠️注意：</sup>"%"和关键字之间的空格不可省  

## 个人补充
通过网络查询，其实还可以使用
* mapper文件中直接使用 '%${key}%' 方式  
* mapper文件中使用concat函数  
&emsp;&emsp;1) concat('%',#{key},'%')
&emsp;&emsp;2) concat('%','${key}','%')  

# PageHelper插件
用来做数据分页的插件，支持多种数据库，如 Oracle/MySQL/MSSQL/DB2/PostgreSQL/etc.  
## 使用
通过maven引入  
MyBatis主配置文件中加到`<enviroments>`标签之前  
```
<plugins>
    <plugin interceptor="com.github.pagehelper.PageInterceptor" />
</plugins>
```
届时在Java代码中使用以下方法进行分页设定  
```
PageHelper.startPage(Integer pageNum, Integer pageSize);
```
它会取出按`pageSize`数量分页的第`pageNum`页数据库表数据  
