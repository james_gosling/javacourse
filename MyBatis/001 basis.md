
## 1. 创建表对象
创建表中每条数据对应的对象类，比如：*org.sean.domain.Student*

## 2. 创建DAO接口
比如：*org.sean.dao.StudentDAO*

## 3. DAO配置文件
也叫做 **mapper文件** 或者 **SQL映射文件**  
在接口目录下创建.xml文件，建议文件名跟接口名保持一致  
示例
```
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE mapper
  PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN"
  "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="org.mybatis.example.BlogMapper">
  <select id="selectBlog" resultType="Blog">
    select * from Blog where id = #{id}
  </select>
</mapper>
```
其中，`mybatis-3-mapper.dtd`文件是该.xml配置文件的约束文件，它会规范该文件中出现的标签  
`mapper`标签：当前文件的根标签，**必须**  
`namespace`属性：命名空间，值需要在文件中具备**唯一**性，可以自定义。**建议**使用DAO接口的全限定名称  
`select`标签：查询动作，类似的还有`update`/`insert`/`delete`  
&emsp;&emsp;`id`属性，值需要在本文件中具备**唯一**性，用来识别一条SQL语句，可以自定义。**建议**使用DAO接口中的方法名称  
&emsp;&emsp;`resultType`属性，表示SQL语句执行后返回的List中存储的结果类型，值则需为对应类的全限定名称  

## 4. MyBatis主配置文件
放在项目的`resources`目录下，为自定义命名的.xml文件  
```
<?xml version="1.0" encoding="UTF-8" ?>
<!DOCTYPE configuration
  PUBLIC "-//mybatis.org//DTD Config 3.0//EN"
  "http://mybatis.org/dtd/mybatis-3-config.dtd">
<configuration>
  <!-- 环境组标签，default用来指定默认使用哪一个环境配置，用它可以快速实现数据库的切换 -->
  <environments default="development">
    <environment id="development">
      <!-- 
        事务类型
        type: JDBC 表示使用jdbc中的Connection对象的commit/rollback做事务处理
      -->
      <transactionManager type="JDBC"/>
      <!--
        数据源
        type:POOLED 表示使用连接池
      -->
      <dataSource type="POOLED">
        <!-- 数据库的连接信息，平时修改这里即可 -->
        <property name="driver" value="${driver}"/>
        <property name="url" value="${url}"/>
        <property name="username" value="${username}"/>
        <property name="password" value="${password}"/>
      </dataSource>
    </environment>
  </environments>
  <mappers>
    <!-- 指定DAO配置文件的路径，比如：com/maven-learn/DAO/StudentDAO.xml -->
    <mapper resource="org/mybatis/example/BlogMapper.xml"/>
  </mappers>
</configuration>
```

## 5. 实际使用MyBatis
使用`SqlSession`对象来执行SQL语句  
```
public class App{
    public static void main(String[] args) throws IOException {
        // MyBatis主配置文件，因在"target/classes/"目录下，所以直接写其名字即可
        String config = "abatis.xml";
        InputStream is = Resources.getResourceAsStream(config);
        SqlSessionFactoryBuilder builder = new SqlSessionFactoryBuilder();
        SqlSessionFactory ssf = builder.build(is);
        SqlSession ss = ssf.openSession();
        // sqlId为 NAMESPACE.SELECT_ID 组成
        String sqlId = "org.javalearn.dao.StudentDAO" + "." + "selectAll";
        // 返回一个行对象列表
        List<Student> studentList = ss.selectList(sqlId);

        studentList.forEach(stu -> System.out.println(stu));
        ss.close();

    }
}
```
### 使用到的类
`org.apache.ibatis.io.Resources`  
&emsp;&emsp;用来读取主配置文件<br>  
`org.apache.ibatis.session.SqlSessionFactoryBuilder`  
&emsp;&emsp;使用主配置文件信息来创建*SqlSessionFactory*<br>  
`org.apache.ibatis.session.SqlSessionFactory`  
&emsp;&emsp;**重量级对象**，创建其耗时较长，使用资源多  
它本身是个`接口`<br>  
`org.apache.ibatis.session.SqlSession`  
&emsp;&emsp;实际用来操作数据库的对象  
也是个`接口`  
