
# 主配置位置
* 输出日志  
以下表示在控制台输出日志
```
<configuration>
    <settings>
        <setting name="logImpl" value="STDOUT_LOGGING" />
    </settings>
</configuration>
```
* 数据库配置文件  
在Java项目目录中创建后缀为`.properties`的文件  
里面内容类似
```
jdbc.driver=org.mariadb.jdbc.Driver
jdbc.url=jdbc:mariadb://localhost:3306/james
jdbc.user=root
jdbc.passwd=197011
```
随后可以在配置文件中将其引入  
```
<configuration>
    <properties resource="THE_PROPERTIES_FILE'S_RELATIVE_PATH" />
</configuration>
```
并在配置文件的`<enviroment> -> <dataSource> -> <property>`里面使用`${key}`的方式进行引用  
* SQL映射文件
`<mapper>`标签也可以使用*resource*键值实现文件引用  
也可以使用`<mappers> -> <package name="PACKAGE_NAME" />`的方式批量引用  
<sup>说明：</sup>使用`package`方式引用需要 1) mapper文件名与接口名保持一致（大小写敏感） 2) mapper文件与DAO接口在同一目录  
在dao文件和mapper文件存放在同一目录且名字一致的情况下，也可以使用`<mapper class="org.javalearn.dao.Student" />`的形式来引入SQL映射<sup>用得不多</sup>

# <settings>标签
据说基本不用  
比如它可以对查询进行优化，但是  
数据量小时，不需要优化；数据量大时，优化效果不明显  
所以对于数据量大时的查询优化：
* 基础操作  
&emsp;&emsp;数据库表设计上做优化，设置索引（比如：普通索引/位图索引/etc.）  
* 高级操作  
&emsp;&emsp;使用*nosql*数据库，比如`redis`  
* 专业操作  
&emsp;&emsp;使用搜索引擎，比如`Solr`或者`Elasticsearch`（针对电商/银行项目）  
