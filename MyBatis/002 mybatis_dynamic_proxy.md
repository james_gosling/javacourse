
按照前文中提到的规范，  
DAO接口下建立同名的.xml mapper文件  
mapper文件里面，`namespace`使用**接口的全限定名称**，各数据表操作语句的`id`使用**接口中的实际名称**。
即可使用`INTERFACE_NAME in = sqlSession.getMapper(INTERFACE_NAME.class);`来动态创建接口实现类  
届时，程序员只需直接调用接口中的方法  
<sup>⚠️注意：</sup>使用动态代理时，DAO接口中&nbsp;**不要**&nbsp;使用<u>方法重载</u>
