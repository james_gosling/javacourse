
# Intro
它制定了：  
互联网通信开发过程中必须出现的角色  
* 有哪些  
* 担负职责  
* 出场顺序  

# 角色
## DAO对象
一个DAO对象对应一张数据库表  
它用来降低对表文件的操作难度，避免反复编写表文件操作代码，提高代码复用性  

## Service对象
提供业务的具体解决方案  
### 业务
浏览器/用户向服务器发起的请求，即为“业务”  

# MVC对象
## Controller object
控制层对象：servlet  
&emsp;&emsp;**可以** 调用`请求对象`来读取请求包中的信息  
&emsp;&emsp;**必须** 调用`Service对象`处理业务  
&emsp;&emsp;**可以** 将处理结果写入`作用域对象`中作为共享数据  
&emsp;&emsp;**必须** 调用`视图对象`将结果写入响应体

## Model object
业务模型对象：service  
&emsp;&emsp;处理业务中所有分支任务  
&emsp;&emsp;根据分支任务执行情况判断业务处理成功与否  
&emsp;&emsp;**必须** 通过*return*将处理结果返回给`控制层对象`  

## View object
视图层对象：jsp 或者 HttpServletResponse  
&emsp;&emsp;**禁止** 参加业务处理  
&emsp;&emsp;**唯一任务**： 将处理结果写入响应体  
