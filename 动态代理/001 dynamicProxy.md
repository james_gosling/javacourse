
# 实现方式
* jdk动态代理  
使用java反射包中的类和接口实现动态代理  
**java.lang.reflect**下的`InvocationHandler`、`Method`、`Proxy`  
其中`InvocationHandler`是接口，内有方法`invoke()`，代理对象需要实现的功能则通过它来实现  
```
/*
* Object proxy:     jdk创建的代理对象，无需赋值
* Method method:    目标类中的方法，jdk提供，同样无需赋值
* Object[] args:    目标类方法的参数，也是jdk提供
*/
public Object invoke(Object proxy, Method method, Object[] args) {};
```
通过`Method`类，可以使用其`invoke()`方法执行目标类的方法&nbsp;<sup>注：</sup>该`invoke()`是**Method**类的，只是恰巧和**InvocationHandler**接口的方法同名了而已  
`Proxy`类用来创建代理对象，使用其静态方法`newProxyInstance()`实现  
* cglib动态代理  
**cglib**是一个第三方工具库，*Code Generation Library*  
其原理是*继承*，通过继承目标类，创建其子类，重写父类中的同名方法，实现动态代理（即方法的修改）  
对于未实现接口的类，若要为其创建动态代理，则无法使用jdk动态代理方式  

# 实现步骤
1. 创建接口，定义目标类要完成的功能
2. 创建目标类实现接口方法
3. 创建`InvocationHandler`接口实现类，在其`invoke()`方法中实现代理类的功能
4. 使用`Proxy`内的静态方法，创建代理对象；并把返回值转为接口类型  

