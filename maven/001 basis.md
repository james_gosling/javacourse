
# 目录结构
该目录结构为约定格式，你可以不遵循
```
Project
    |--pom.xml                  # maven核心文件（必需）
    |--src/
    |     |--main/               # 主程序文件夹
    |     |    |--java/          # 包、java文件
    |     |    |--resources/     # 配置文件
    |     |--test/               # 测试程序文件夹
    |          |--java/          # 包、java文件
    |          |--resources/     # 配置文件
    |--target/                  # maven编译生成的 .class 文件存放在这里
```

# 命令
`mvn compile`  
&emsp;&emsp;编译当前项目的*main*目录下所有的**java**文件  
`mvn clean`  
`mvn test-compile`  
`mvn test`  
&emsp;&emsp;会顺序执行*clean*、*compile*、*test-compile*，然后执行测试类中的**\@Test**方法，最后输出报告  
`mvn package`  
`mvn install` 
&emsp;&emsp;将该项目安装到本地仓库中   
`mvn deploy`  
&emsp;&emsp;部署，intro里提到过  

# 仓库

# `pom.xml`
POM == Project Object Model(项目对象模型)<br>
***modelVersion***  
&emsp;&emsp;Maven项目模型的版本，Maven2 和 Maven3 均为 4.0.0<br>
***groupId*** & ***artifactId*** & ***version***  
&emsp;&emsp;分别是 **组织id**、**模块名称**、**版本号**  
组织id 多为 域名倒写，比如 com.apache  
模块名称 则是 具体的一个项目，比如 maven  
版本号 即为 项目版本，例如 0.0.1，开发中的项目，后面常会带 **-SNAPSHOT**<br>
***packaging***  
&emsp;&emsp;项目打包的类型，可以是jar/war/rar/ear/pom，默认是 jar<br>
***dependencies*** & ***dependency***  
&emsp;&emsp;指定项目的依赖库  
形如
```
<dependencies>
    <!-- https://mvnrepository.com/artifact/org.mariadb.jdbc/mariadb-java-client -->
    <dependency>
        <groupId>org.mariadb.jdbc</groupId>
        <artifactId>mariadb-java-client</artifactId>
        <version>2.7.3</version>
    </dependency>
</depedencies>
```
***properties***  
&emsp;&emsp;用来指定maven项目的全局变量  
常用的有如：`project.build.sourceEncoding`、`maven.compile.source`、`maver.compile.target`  
也可以用来定义你在pom文件中会用到的自定义变量，在需要的地方使用`${TAG_NAME}`的方式进行引用    
***build***  
&emsp;&emsp;里面的`<resources>`标签可以添加`<resource>`  
`<resource>`标签里可以通过`<directory>`、`<includes>`、`<filtering>`标签来告知maven编译时将指定目录中的匹配文件一并复制到编译文件夹中    
***parent***  
&emsp;&emsp;  
***modules***  
&emsp;&emsp;  

## 依赖范围
`<dependency>`标签中可以有`<scope>`标签，用来表示该依赖的作用范围  
比如 junit ，它的默认scope使用的是**test**，表示它只在测试阶段会被使用  
scope可使用的值为 *compile*/*test*/*provided* ，默认是 **compile**  
**provided** 表示 该jar包在 编译、测试 时会被使用，但是 打包、安装 时则不需要，比如 *javax.servlet* 这个依赖就使用 provided  

