
# 概念
**Apache**基金会推出的一个*软件项目管理工具*  
它可以自动地帮你处理项目开发中用到的各种依赖库  
借助于它，可以实现项目的 清理/编译/测试并输出报告/打包/安装/<del>部署</del><sup>说明：这项用得比较少，因为手动更方便</sup>  
