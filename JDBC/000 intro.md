[TOC]

# What is JDBC
**JDBC** = Java DataBase Connectivity  
它是Sun公司制定的一套接口，用于帮助Java程序使用数据库  
各数据库厂家根据Sun公司提供的JDBC接口，编写其实现类，最终产生.jar形式的类的集合，称作*驱动*

# JDBC编程6步

1. 注册驱动<sup>&emsp;告知Java将连接的是谁家的数据库</sup><br>
2. 获取连接<sup>&emsp;JVM进程和数据库进程之间的数据通道，进程之间的通信（重量级），故需要第6步释放资源动作</sup><br>
3. 获取数据库操作对象<br>
4. 执行SQL语句<sup>&emsp;ex:DQL、DML</sup><br>
5. 处理查询结果集<sup>&emsp;只有执行语句是*select*的时候才会有的动作</sup><br>
6. 释放资源<br>

```
import java.sql.*;

public static void main(String[] args) {
    Connection conn = null;
    Statement stmt = null;
    try{
        // 👉Step 1
        /*
        * 这是传统注册方式
        * DriverManager.registerDriver(new com.mysql.jdbc.Driver());
        */
        // 这是常用的方式
        // 字符串可以写到配置文件当中去
        Class.forName("com.mysql.jdbc.Driver");

        // 👉Step 2
        // MySQL默认端口3306
        String url = "jdbc:mysql://IP_ADDR:PORT/DATABASE_NAME";
        String user = "USER_NAME";
        String password = "PASSWORD";
        conn = DriverManager.getConnection(url, user, password);

        // 👉Step 3
        stmt = conn.createStatement();

        // 👉Step 4
        // 不是select语句所以没有Step 5
        String sql = "insert into dept(deptno, dname, loc) values(50, 'sales', 'shanghai')";
        // executeUpdate()方法会返回int数据，表示此条sql执行关联的数据条数
        stmt.executeUpdate(sql);
    }catch(SQLException e){
        e.printStackTrace();
    }finally{
        // 👉Step 6
        if(stmt != null) {
            try{
                stmt.close();
            }catch(SQLException e){
                e.printStackTrace();
            }
        }

        if(conn != null) {
            try{
                conn.close();
            }catch(SQLException e){
                e.printStackTrace();
            }
        }

    }
}
```
**insert**/**update**/**delete** 都只需要一个 Statement 对象即可  
**select** 还需要一个 ResultSet 对象用来接收查询结果，对应于`executeUpdate`，它使用`executeQuery`方法

```
ResultSet rs;
// rs.next() 用来将rs的指针下移一行，如果有数据，返回 true ，否则返回 false
// rs.getString(int index) 用来获取当前rs指向行的数据，index从1开始
// getString(String col_name) 可以通过(查询结果)列名来取数据（相比于index方式更健壮）
// 如果SQL语句中对于列用了as语法，那么col_name应该是'as'后面接的字符串，而不是数据库表中的字段名
// 类似地，rs也有getInt、getDouble方法
```

## 扩展 - SQL注入

`Statement` 可能会被非法分子执行**SQL注入**操作  
`PreparedStatement` 可以有效避免该风险，其原理是它会预编译SQL语句框架，之后再给SQL语句传参  
它是一个接口，使用`Connection`对象的`prepareStatement()`方法可以
```
// 定义SQL语句框架，其中代传参部分使用'?'占位符代替
String sql = "select * from tbl_user where username = ? and userid = ?";
// 数据库连接对象对SQL语句框架预编译，获取数据库操作对象
PreparedStatement ps = conn.prepareStatement(sql);
// 数据库操作对象具体对占位符传参
// 除了setString，还有传其它类型数据的方法，如setInt
ps.setString(1, String param1);
ps.setString(2, String param2)
```

### Statement 和 PreparedStatement 对比

* Statement 存在SQL注入安全隐患，PreparedStatement 则没有

* 执行效率上，PreparedStatement 会更高（SQL查询缓存的缘故？）

* PreparedStatement 可以实现值的类型检查（其实就是setString只接收String类型的数据，并没有涉及数据库数据的类型判断）

所以，大多数情况下都是用 PreparedStatement ，但是 Statement 也有使用的时候，比如：  
搜索结果需要升序/降序显示，这种情况用 Statement 可以更简单实现。即某些需求需要使用SQL注入时。

## ⭐️⭐️⭐️事务处理

默认数据库操作对象是让它执行语句，完成之后它就提交  
这种操作方式明显不符合传统业务模式  
Connection 对象可以调用`setAutoCommit(boolean flag)`方法，传 false 给它表示**禁用**自动提交  
关闭自动提交后，需要使用Connection对象的`commit()`方法进行**手动**提交  
同理，回滚则调用其`rollback()`方法
