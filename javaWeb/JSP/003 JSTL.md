
# 概念
`JSTL` = Java Standard Tag Lib  
通常结合EL表达式一起使用  
目的是让JSP中的Java代码消失  

# 使用
## 1. 引入jar包
讲对应jar包引入项目<sup>note: </sup>Tomcat10开始跟之前版本引入的jar文件不太一样（之前是<del>jstl.jar和taglibs-standard.jar</del>1.2版本的jstl.jar、jstl-api.jar, 之后是<del>jstl.jar、standard-impl.jar、standard-spec.jar</del>2.0版本的两个文件）  

## 2. JSP中引入标签库
```
<!--
    uri     决定使用哪个标签库，示例中的jstl/core较常用
    prefix  随意，是个自定义的值，随后使用JSTL时会用到它
            一般都会用"c"
    
-->
<%@taglib prefix="j" uri="http://java.sun.com/jsp/jstl/core" %>
```

## 3. 实际使用
### forEach
用来遍历集合
```
<!--
    items   用来指代用来迭代的对象
    var     用来跟每次迭代的具体项设定别名
    forEach标签体内支持EL表达式，所以直接使用var设定的变量名来取该对象的属性
-->

<j:forEach items="${stuList}" var="s">
    id:${s.id}, name:${s.name}
</j:forEach>
```

### if
用来做判断
```
<!--
    test    必须项，if的判断条件
            可以使用EL表达式
JSTL中没有else标签，所以如果要实现分支判断，需要使用多个if
-->
<j:if test="">
</j:if>
```

### choose
多分支判断
```
<j:choose>
    <j:when test="">

    </j:when>
    <j:when test="">

    </j:when>
    <j:when test="">

    </j:when>
    <j:otherwise>

    </j:otherwise> 
</j:choose>
```

# 原理
JSP中引入标签库中使用的uri(即类似"http://java.sun.com/jsp/jstl/core"的字段)实际指向对应jar包中的一个tld文件  
tld文件本身为xml格式  
它定义了"标签"和"Java类"之间的对应关系  
