
# 基本样式
以下为使用IDEA自动生成的jsp文件所含的主要内容
```
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>Title</title>
  </head>
  <body>
  
  </body>
</html>
```

# 书写Java命令
```
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    // 这里就可以写实际的Java命令，两头的固定标记称之为“执行标记”

    /*
    * 在这里我们可以
    * 声明变量、声明数学/关系/逻辑运算表达式、声明控制语句
    */

%>

<%--
    下面是输出标记，用来直接输出Java变量到响应体
    除了直接写变量，它也可以写运算式，比如：var1+var2
--%>
<%=var%>

<%-- 导包 --%>
<%@ page import="com.javacourse.package.class" %>
```

# JSP常用的内置Java对象
* request  
&emsp;&emsp;类型：HttpServletRequest  
&emsp;&emsp;作用：  
&emsp;&emsp;+ 在JSP文件运行时读取请求包中的信息  
&emsp;&emsp;+ 与Servlet实现类在请求转发过程中实现数据共享

* session  
&emsp;&emsp;类型：HttpSession  
&emsp;&emsp;作用：  
&emsp;&emsp;JSP文件运行时读取/写入session数据

* application  
&emsp;&emsp;类型：ServletContext  
&emsp;&emsp;作用：  
&emsp;&emsp;与同一个网站中的Servlet实现类和JSP文件实现数据共享

# JSP 与 Servlet 之间的分工
Servlet：  
&emsp;&emsp;负责处理业务请求，得到最终结果  
JSP：  
&emsp;&emsp;负责将Servlet的处理结果写入响应体  

Servlet完成业务处理后，使用`请求转发`方式调用JSP文件

# 运行原理
1. Http服务器将JSP文件转化为一个Servlet接口实现类<br>
名为**example.jsp**的文件会转化为**example_jsp.java**文件
2. 将得到的实现类编译为class文件
3. 用class文件创建实例对象
4. 调用对象的`_jspService()`方法，将JSP内容写入响应体
