
# 介绍
使用Java技术开发的一个jar包  
目的是**降低JSP文件开发时，编写Java命令的强度**  
Tomcat自带，路径在`TOMCAT_DIR/lib/el-api.jar`

# 命令格式
`${作用域对象别名.共享数据名}`，对于共享数据是引用数据类型，格式为`${作用域对象别名.共享数据名.属性名}`<br>
<sup>补充：</sup>如果共享数据是数组，可以使用`共享数据名[下标]`进行具体对象的访问<br>
<sup>补充2：</sup>EL表达式没有遍历集合的方法，所以Map、Set等集合的内容无法读取，该缺陷可以由JSTL来补足

## 示例
```
// 获取全局作用域对象属性
${applicationScope.var1}
// 获取会话作用域对象属性
${sessionScope.var2}
// 获取请求作用域对象属性
${requestScope.var3}
```

# 可使用的作用域对象
* 之前提到的3个，`ServletContext`、`HttpSession`、`HttpServletRequest`
* `PageContext`：只在JSP文件中出现的作用域对象，称之为**当前页作用域对象**<br>
它里面的数据只能由本JSP文件使用<br>
主要用于**JSTL标签**与**JSP文件**之间共享数据<br>
所以，pageContext里面的数据应该是由JSTL标签写入，JSP负责取出它们，写入响应体

## 作用域别名
`applicationScope`、`sessionScope`、`requestScope`、`pageScope`

# 简化格式
`${共享数据名}`

## 弊端
数据同名时可能读到的不是你想要的结果，它使用'猜'算法来拿数据  
按照顺序，从 pageContext -> request -> session -> application 里面去找数据，最先找到即返回；若都没有，返回*null*

# 支持的运算符
`数学运算`(+ - * / %)、`关系运算`(> gt >= ge == eq <= le < lt)、`逻辑运算`(&& || !)

# 内置对象
`${param.请求参数名}`  
用处：读取请求包中的内容，写到响应体中  

`${paramValues.请求参数名[下标]}`  
用处：用来处理请求包中一个参数名关联多个值的数据(比如checkbox)
