

用处：<br>
监听器接口用于监控`作用域对象生命周期变化时刻`和`作用域对象共享数据变化时刻`

作用域对象：<br>
Servlet规范中，在服务端内存中，某种条件下可以为两个Servlet对象提供数据共享方案的对象，称之为`作用域对象`<br>
所以，之前笔记里提到的`ServletContext`、`HttpSession`、`HttpServletRequest`的实例化对象都是作用域对象

监听器接口实现类开发规范：<br>
1. 根据实际需要，选择对应的监听器接口
2. 实现接口申明的`监听事件处理方法`
3. 在`web.xml`中注册监听器接口实现类  
```
<listener>
    <listener-class>com.jd.listener.ANewListener</listener-class>
</listener>
```

所属包：<br>
**listener**<br>
其实现类一般会放在名为'listener'的包下

# ServletContextListener 接口
该接口用来监听全局作用域对象的初始化、销毁时刻<br>
* contextInitlized()

* contextDestroy()

# ServletContextAttirbuteListener 接口
见名知义，用来监听全局作用域对象共享数据的变化<br>
* contextAdded()

* contextRepalced()

* contextRemoved()

使用场景：<br>
* 连接池<br>
Connection 对象的创建是比较影响用户体验的(耗时)<br>
可以使用 ServletContextListenre 在网页加载时就创建连接池<br>
需要数据库连接时直接从 ServletContext 中去获取即可
