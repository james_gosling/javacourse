> 为什么要做Servlet之间的调用？
> 
> 提升用户体验<br>
> 虽然某个请求需要涉及多个Servlet的调用<br>
> 但是我们可以让用户做到&nbsp;**一个请求，多次调用**

# 重定向
实现方式：
```
response.sendRedirect("/WEBSITE_NAME/SERVLET_NAME");
```
特征：
* 既可以访问本站资源`/站点名/资源文件名`，也可以访问互联网中的其它资源`http://www.bing.com`
* 用户手动发送一次请求，浏览器至少发送两次请求
* 重定向的请求方式为**GET**

缺点：
* 浏览器与服务器之间多次数据传递，增加用户等待时间

# 请求转发
原理：<br>
一个Servlet完成其工作后，代替客户端浏览器向HTTP服务器发送调用下一个Servlet的请求

实现方式：
```
RequestDispatcher rd = request.getRequestDispatcher("/RESOURCE_PATH");
rd.forward(request, response);
```
特征：
* 浏览器只发送一次请求
* 调用`doGet`还是`doPost`是由浏览器请求决定的
