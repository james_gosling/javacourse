
用途：<br>
在Http服务器调用资源文件之前，对其进行拦截，从而
* 判断请求的合法性
* 对当前请求执行附加动作

实现规范：<br>
1. 创建一个Filter接口的实现类
2. 实现其`doFilter()`方法
3. `web.xml`文件中注册该实现类
```
<filter>
    <filter-name>aCustomizedNameHere</filter-name>
    <filter-class>com.javacourse.filter.AFilter</filter-class>
</filter>
<filter-mapping>
    <filter-name>aCustomizedNameHere</filter-name>
    <!-- 允许使用通配符，比如'/*'就表示调用任何资源时都会使用该过滤器做处理 -->
    <url-pattern>/SOURCE_PATH</url-pattern>
</filter-mapping>
```

所属包：<br>
**filter**

实现范例：<br>
```
public class AFilter implements Filter {
    public void doFilter(ServletRequest req, ServletResponse resp, FliterChain fc) throws IOException, ServletException {
        // 获取判断用参数

        /*
        * 规则判断
        */
        if() {
            // ...
            fc.doFilter(req, resp);  // 对请求放行
        } else {
            // ...
        }
    }
}
```

拦截pattern写法：<br>
* 具体资源路径<br>
比如 '/img/A.png'
* 指定文件夹
比如 '/img/*'
* 指定文件类型
比如 '*.jpg'
* 调用网站任意资源
'/*'
