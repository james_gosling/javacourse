[TOC]

# ServletContext接口
别名：<br>
&emsp;&emsp;全局作用域对象

原理：<br>
Http服务器启动时，会自动为其管理的各网站各建一个全局作用域对象<br>
Http服务运行期间，该对象一直存在；直到Http服务停止，它会销毁其创建的全局作用域对象们

使用：
```
// 存入
ServletContext application = request.getServletContext();
application.setAttribute(String key, Object value);

// 取出
application.getAttribute(String key);
```

<sup>备注：</sup>因为一个网站只有一个，放太多数据会存在占用资源甚至导致网站宕机的风险<br>
&emsp;&emsp;&ensp;故一般高级程序员才有资格往全局作用域对象中写数据，普通的CV工程师只配从中读取😁

# Cookie类
原理：<br>
给同一个浏览器/用户提供服务的Servlets，可以使用它来进行数据共享<br>
用于存放当前用户的私人数据，以便提高服务质量<br>
Cookie会被写在`响应头`中交还给客户端浏览器，由其存储在它的缓存中<br>
之后再由该浏览器访问该网站时，浏览器会无条件将缓存的Cookie写入`请求头`，该网站的Servlet通过该Cookie，获取网站共享的用户私人数据

使用：
```
// 生成Cookie并存入数据
/*
* 一个Cookie对象只能存储一个键值对
* 键、值均为String类型数据
* 键 不可以 是 中文
*/
Cookie c = new Cookie(String key, String value);
resp.addCookie(c); // 如果需要多个Cookie，再调该方法即可

// 使用Cookie
Cookie[] cookies = req.getCookies();  // 获取Cookie集合
for(Cookie c:cookies) {
    c.getName();    // 获取key
    c.getValue();   // 获取value
}
```

生命周期：<br>
默认情况下，发起请求的浏览器关闭，Cookie对象就会被销毁<br>
但是可以手动指定Cookie对象的存活时间，这样客户端浏览器会将其保存在硬盘上，直到存活时间耗尽
```
cookie.setMaxAge(60);  // 指定Cookie存活1分钟
```

# HttpSession接口
别名：<br>
&emsp;&emsp;会话作用域对象

原理：<br>
给同一个浏览器/用户提供服务的Servlets，可以使用它来进行数据共享

与Cookie的区别：<br>
* 存储位置<br>
HttpSession存放在服务端计算机的内存中；Cookie存放在客户端计算机的浏览器内存或者硬盘中

* 存储数据类型<br>
HttpSession可以存放任意类型的数据；Cookie只能存放String类型

* 存储数据数量<br>
一个Cookie对象只能存储一对键值对；HttpSession使用map集合，可以存放任意数量的数据

使用：
```
// 拿到会话，执行操作
HttpSession session = request.getSession();
session.setAttribute(String key, Object data);

// 取用数据
Object data = session.getAttribute(String key);     // 知道要取什么键时使用
Enumeration keys = session.getAttributeNames();     // 取出所有键
```

如何与Cookie结合：<br>
客户端浏览器第一次发起请求时，若Servlet使用到了HttpSession<br>
那么在响应包中，Tomcat服务器会写入`JSESSIONID`字段的cookie<br>
下次该浏览器向同一网站发起请求时，请求包中会将该cookie再传给服务器<br>
从而实现两者的结合使用

`getSession()` 与 `getSession(false)`<br>
两者的区别在于 判断服务端是否已有该请求的session<br>
前者如果没有，会为请求创建<br>
后者如果没有，不动作(返回值为null)<br>
&emsp;&emsp;**如何选用？**<br>
如果之前已有身份验证，使用 getSession()<br>
如果之前没有，使用 getSession(false)

生命周期：<br>
HttpSession关联使用的Cookie(JSESSIONID)只能存放于**浏览器缓存**中，所以浏览器关闭时，该cookie丢失，用户与该session的关系就不存在了<br>
但是Tomcat服务器并不知道客户端浏览器关闭的动作，它为每个HttpSession对象设置了**空闲时间**，默认为`30分钟`，一旦30分钟内HttpSession都没被使用，Tomcat会主动销毁该session<br>
如需设定该空闲时间，编辑`WEB_DIR/web/WEB-INF/web.xml`文件
```
<session-config>
    <!-- 单位是：分钟 -->
    <session-timeout>5<session-timeout>
</session-config>
```

# HttpServletRequest接口
原理：<br>
两个Servlet通过`请求转发`实现调用，它们则共享同一个请求包，一个请求包对应一个请求对象，故它们可以利用该请求对象实现数据共享<br>
实现共享的这个请求对象，称之为`请求作用域对象`

使用：
```
// 存数据
request.setAttribute(String key, Object data);

// 取数据
Object data = req.getAttribute(String key);
```
