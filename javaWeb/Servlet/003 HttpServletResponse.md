用来写HTTP响应的接口
<hr>

`getWriter()`方法可以获取一个PrintWriter对象用来写数据

&emsp;调用`write()`/`print()`写数据

&emsp;&emsp;两者区别

&emsp;&emsp;&emsp;write()写int时，会将其当成ASCII码来解析

&emsp;&emsp;&emsp;print()则当成一般数字处理

`setContentType()`

&emsp;响应对象默认的content-type是**text**

&emsp;有时候我们需要根据实际需要修改，使用setContentType()方法，比如`response.setContentType("text/html")`可以指定之后输入的数据使用html解析器进行解析

`sendRedirect()`

&emsp;用来设置相应头的**location**属性
