Servlet其理念是用来处理HTTP动态请求

相比于静态资源，HTTP服务器也需要处理动态请求
比如 京东商城的一次购物请求，会传递本次结账的商品、数量等一系列参数

各HTTP服务器(Tomcat/Jetty/GlassFish/WebLogic/WebSphere/etc.)负责提供Servlet API，而应用实现者则负责将API中的接口、方法实现

Tomcat里面，带有`servlet-api.jar`包，其内有`javax.servlet.Servlet`接口
在基于Tomcat做HTTP应用开发的时候，程序员即通过`extends HttpServlet`来实现其具体的Servlet

Servlet继承类常放在`controller`包下

一般，具体的Servlet类需要实现`doGet`或者`doPost`方法即可

该实现过程为
服务器接收到用户请求，传给**HttpServlet**的`service()`方法
`service()`方法会根据请求的具体方法，调用**HttpServlet**继承类实现的`doGet`或者`doPost`(其实还有`doHead`/`doTrace`/etc. 另外5个方法)

完成Servlet继承类的具体代码后，还需要将Servlet注册到Tomcat服务器
编辑`./web/WEB-INF/web.xml`文件，在其中写入Servlet相关信息
```
<servlet>
    <servlet-name>a_specify_name</servlet-name>
    <servlet-class>com.servlet.controller.TargetClass</servlet-class>
    <!--👇Note:-->
    <load-on-startup>30</load-on-startup>
    <!--👆填写正整数，HTTP服务器启动时就会创建该实现类对象-->
</servlet>

<servlet-mapping>
    <servlet-name>the_specify_name_as_above</servlet-name>
    <!--👇Note:-->
    <url-pattern>/a_alias_that_use_in_URL</url-pattern>
    <!--👆url_pattern以'/'开始-->
</servlet-mapping>
```
