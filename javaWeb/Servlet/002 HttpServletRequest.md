用来读HTTP请求的接口
<hr>

`getRequestURL()`

&emsp;获取请求行中**URL**信息

&emsp;返回的是一个 *StringBuffer* 对象，可以再使用`toString()`方法将其转换为 *String* 对象

`getMethod()`

&emsp;获取请求行中的**method**信息

`getRequestURI()`

&emsp;获取请求*URI*信息

> URI 格式：
> 
> &emsp;/网站名/资源文件名

`getParameterNames()`

&emsp;获取所有请求参数名，返回的是一个 *Enumeration* 对象

`getParameter()`

&emsp;获取请求参数值，返回的是一个 *String* 对象

> 不管请求方式是 get 还是 post
> 
> 都是通过该方法来获取参数信息
>
> 但是 post 中的参数可能解析出乱码
>
> &emsp;原因：
>
> &emsp;&emsp;请求头中的参数由 Tomcat 负责解码，它使用 UTF-8 字符集
>
> &emsp;&emsp;而请求体中的参数由 request 对象解码，它默认使用 ISO-8859-1 字符集
>
> &emsp;解决办法：
>
> &emsp;&emsp;使用`request.setCharacterEncoding("utf-8")`指定 request 使用哪种字符集
